#language: pt
Funcionalidade: Pesquisar Temperatura
  Eu como operador devo acessar a pagina do google chrome e realizar uma pesquisa de temperatura

  Cenario: Ct 001 - Realizar pesquisa de temperatura de qualquer cidade
    Dado eu estou na pagina inicial do google "https://www.google.com.br"
    Quando eu devo preencher no campo busca a palavra "São Paulo"
    Entao devera ser apresentado a temperatuda da cidade
    E devera capturar imagem do resultado