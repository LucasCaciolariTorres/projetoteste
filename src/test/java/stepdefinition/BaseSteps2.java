package stepdefinition;

import br.com.link.setup.ConfigFramework;
import br.com.link.setup.Drivers;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;

import static br.com.link.setup.ExtentReports.appendToReport;
import static br.com.link.view.Actions.getText;
import static br.com.link.view.Actions.setText;


public class BaseSteps2 extends ConfigFramework {

    @Dado("^estou na página principal da lojas americanas \"([^\"]*)\"$")
    public void estou_na_página_principal_da_lojas_americanas(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Drivers.loadApplication(getBrowser(),arg1,true);
    }

    @Quando("^eu pesquisar no campo de busca a palavra \"([^\"]*)\"$")
    public void eu_pesquisar_no_campo_de_busca_a_palavra(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        setText(getBrowser(), By.id("h_search-input"), arg1, 10);
    }

    @Entao("^devera ser apresentado uma lista de bikes$")
    public void devera_ser_apresentado_uma_lista_de_bikes() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        getBrowser().findElement(By.id("h_search-input")).submit();
    }

    @E("^devera ser validado o valor mais barato da lista$")
    public void deveraSerValidadoOValorMaisBaratoDaLista() throws Throwable {
        // Write code here that turns the phrase above into concrete action
        float preco = 0;
         int x = getBrowser().findElements(By.cssSelector("div.row.product-grid.no-gutters.main-grid > div")).size();
         float[] preco2 = new float[25];
         float aux = 0;

        for(int i = 1; i<=x;i++){
            String text = getText(getBrowser(), By.cssSelector("div.product-grid > div:nth-of-type("+i+") div:nth-of-type(2) > div:nth-of-type(2) > div:nth-of-type(2) > span:nth-of-type(1)"),40);
            text = text.replaceAll("[^0-9]", "");
            preco = Float.parseFloat(text);
            preco2[i] = preco / 100;
            appendToReport(getBrowser(),"Preço do item "+i+": "+preco2[i], false);
        }

        aux = preco2[1];
        for(int i = 2; i<=x;i++){
            if(aux>preco2[i]){
                aux = preco2[i];
            }
        }
        appendToReport(getBrowser(),"O menor preço é: "+aux,false);
    }
}




