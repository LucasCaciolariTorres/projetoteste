package stepdefinition;

import br.com.link.setup.ConfigFramework;
import br.com.link.setup.Drivers;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.By;

import static br.com.link.setup.ExtentReports.appendToReport;
import static br.com.link.view.Actions.*;


public class BaseSteps extends ConfigFramework {

    @Dado("^eu estou na pagina inicial do google \"([^\"]*)\"$")
    public void eu_estou_na_pagina_inicial_do_google(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Drivers.loadApplication(getBrowser(), arg1, true);
    }

    @Quando("^eu devo preencher no campo busca a palavra \"([^\"]*)\"$")
    public void eu_devo_preencher_no_campo_busca_a_palavra(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        setText(getBrowser(), By.name("q"), "Temperatura de Rio de janeiro",2);
    }

    @Entao("^devera ser apresentado a temperatuda da cidade$")
    public void devera_ser_apresentado_a_temperatuda_da_cidade() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        clickOnElement(getBrowser(), By.name("btnK"),10);
    }

    @E("^devera capturar imagem do resultado$")
    public void devera_capturar_imagem_do_resultado() throws Throwable{
        // Write code here that turns the phrase above into concrete actions
        String temperatura = getText(getBrowser(), By.id("wob_tm"), 10);
        appendToReport(getBrowser(), "A temperatura de RJ e: " + temperatura,true);
    }

}
