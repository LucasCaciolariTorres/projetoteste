package hooks;

import br.com.link.setup.AppWeb;
import br.com.link.setup.ExtentReports;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import static br.com.link.setup.Drivers.closeDriver;

public class Hook extends ExtentReports {

    @Before
    public void init() {
        super.setCucumberLanguage();

        /*Exemplo de start de driver para desktop
        AppWindows app = new AppWindows(getDesktop());
        app.setUpDriver(app);
        setDesktop(app.getDriver());*/

        /*Exemplo de start de driver para navegador*/
        AppWeb app = new AppWeb(getBrowser());
        app.setUpDriver(app);
        setBrowser(app.getDriver());
		
		/*Exemplo de start de driver Android
        AppAndroid app = new AppAndroid(getAndroidDriver());
        app.setUpDriver(app,null);
        setAndroidDriver(app.getDriver());*/
    }

    @After
    public void cleanUp(Scenario scenario) {
        super.logReport(getBrowser(),scenario); 
		super.logReport(getDesktop(),scenario);
		super.logReport(getAndroidDriver(), scenario);

        closeDriver(getBrowser()); closeDriver(getDesktop());
    }

}
