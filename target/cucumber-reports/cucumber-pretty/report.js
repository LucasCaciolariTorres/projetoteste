$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/template2");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Consultar Produtos",
  "description": "Eu devo acessar o site da americanas.com e obter o produto mais barato",
  "id": "consultar-produtos",
  "keyword": "Funcionalidade"
});
formatter.before({
  "duration": 8899800886,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "CT 001 - Consultar produto com menor preço",
  "description": "",
  "id": "consultar-produtos;ct-001---consultar-produto-com-menor-preço",
  "type": "scenario",
  "keyword": "Cenario"
});
formatter.step({
  "line": 7,
  "name": "estou na página principal da lojas americanas \"https://www.americanas.com.br\"",
  "keyword": "Dado "
});
formatter.step({
  "line": 8,
  "name": "eu pesquisar no campo de busca a palavra \"bike\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 9,
  "name": "devera ser apresentado uma lista de bikes",
  "keyword": "Entao "
});
formatter.step({
  "line": 10,
  "name": "devera ser validado o valor mais barato da lista",
  "keyword": "E "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.americanas.com.br",
      "offset": 47
    }
  ],
  "location": "BaseSteps2.estou_na_página_principal_da_lojas_americanas(String)"
});
formatter.result({
  "duration": 6170189864,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "bike",
      "offset": 42
    }
  ],
  "location": "BaseSteps2.eu_pesquisar_no_campo_de_busca_a_palavra(String)"
});
formatter.result({
  "duration": 1626788273,
  "status": "passed"
});
formatter.match({
  "location": "BaseSteps2.devera_ser_apresentado_uma_lista_de_bikes()"
});
formatter.result({
  "duration": 5558711686,
  "status": "passed"
});
formatter.match({
  "location": "BaseSteps2.deveraSerValidadoOValorMaisBaratoDaLista()"
});
formatter.result({
  "duration": 2517652765,
  "status": "passed"
});
formatter.after({
  "duration": 854893753,
  "status": "passed"
});
});