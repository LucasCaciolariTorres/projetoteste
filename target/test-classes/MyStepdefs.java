import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class MyStepdefs {
    @Dado("^eu estou na pagina inicial do google \"([^\"]*)\"$")
    public void euEstouNaPaginaInicialDoGoogle(String arg0) throws Throwable {
        /* Write code here that turns the phrase above into concrete actions */
    }

    @Quando("^eu devo preencher no campo busca a palavra \"([^\"]*)\"$")
    public void euDevoPreencherNoCampoBuscaAPalavra(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }

    @Entao("^devera ser apresentado a temperatuda da cidade$")
    public void deveraSerApresentadoATemperatudaDaCidade() {
    }

    @E("^devera capturar imagem do resultado$")
    public void deveraCapturarImagemDoResultado() {
    }


}
